import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import matplotlib.colorbar
import os
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature
from wrf import (getvar, interplevel, to_np, latlon_coords, get_cartopy,
                 cartopy_xlim, cartopy_ylim, smooth2d)

def PressureMapPrecisely(filename, p, time, lats, lons, z, u, v, windspeed):
	fig = plt.figure(figsize=(12,6))
	
	NY_east = -73
	NY_west =  -80
	NY_north = 46.5
	NY_south = 39
	level = input('What pressure level would you like to use?')
	
	ht = interplevel(z, p, level)
	u = interplevel(u, p, level)
	v = interplevel(v, p, level)
	wspd = interplevel(windspeed, p, level)
	cart_projPress = get_cartopy(ht)
	smoothPress = smooth2d(ht, 2)
	ax = fig.add_subplot(1, 1, 1, projection=crs.PlateCarree())
	ax.set_extent([NY_east, NY_west, NY_south, NY_north])
	states = NaturalEarthFeature(category="cultural", scale="50m",facecolor="none",name="admin_1_states_provinces_shp")
	
	
	ax.add_feature(states, linewidth=2, edgecolor="black")
	ax.coastlines('50m', linewidth=2)
	ax.gridlines(color="black", linestyle="dotted")
	
	plt.contour(to_np(lons), to_np(lats), to_np(smoothPress), 5, colors="black",transform=crs.PlateCarree(), linewidth=.25)
	plt.contourf(to_np(lons), to_np(lats), to_np(smoothPress), 5,transform=crs.PlateCarree(), linewidth=.25)
	
	ax.barbs(to_np(lons[::5,::5]), to_np(lats[::5,::5]), to_np(u[::5, ::5]), to_np(v[::5, ::5]), transform=crs.PlateCarree(), length = 6)
	
	plt.colorbar(ax=ax, shrink=.98)
	
	plt.title(f"Pressure at {level} hPA")
	a = input('Enter a file name with .png')
	
	
	plt.savefig(a)
	plt.show()
	plt.close()