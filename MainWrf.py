####

#### Main body of the Code
#Test Case to understand modules

from netCDF4 import Dataset
import numpy as np
import importlib
import os
from netCDF4 import Dataset
from Near import nearidx
from newFile import newFile
from wrf import getvar, get_cartopy, latlon_coords, geo_bounds, smooth2d
from MapperTemp import TempMap
from MapperTemp2 import TempMap2
from MapperSLP import SLPMap
from PressureMapper import PressureMap
from WindMapper2 import WindMap
from PrecipMapper import Precip
from PrecipMapper2 import Precip2
from tester101 import PressureMapPrecisely
from inPressureMap import PressureMapChooser, CTT
from urllib.request import urlopen
from datetime import datetime, timedelta

# '''NWS Calculations found from dt = datetime.utcnow() - timedelta(days=1)  # This should always be available
#'http://water.weather.gov/precip/downloads/{dt:%Y/%m/%d}/nws_precip_1day_'\
# Open Code used from https://unidata.github.io/python-gallery/examples/Precipitation_Map.html     
# 
#
#dt = datetime.utcnow() - timedelta(days=1)  # This should always be available
#url = 'http://water.weather.gov/precip/downloads/{dt:%Y/%m/%d}/nws_precip_1day_'\
#      '{dt:%Y%m%d}_conus.nc'.format(dt=dt)
#data = urlopen(url).read()
#file1 = Dataset('', memory=data)



#fin = 'nws_precip_1day_20190719_ak.nc'
fin = "wrfout_d01_2020-07-23_00:00:00"
#-  read input file
#print("############") 
#print("INFILE",fin) 

file1 =  Dataset(fin, "r", format="NETCDF4")

#---get attributes from all the variables

time = getvar(file1, "times")
try:
	lon = getvar(file1, "lon")
	lat = getvar(file1, "lat")
except:
	lon = getvar(file1, "XLONG")
	lat = getvar(file1, "XLAT")
td = getvar(file1, "td")
temp = getvar(file1, "T2")
slp = getvar(file1, "slp")
qv = getvar(file1, "QVAPOR")
qr = getvar(file1, "QRAIN")
p = getvar(file1, "p")
tempF = (getvar(file1, 'T2')- 273.15)*(9/5)+32
temp3d = getvar(file1, "temp")
cloudTT = getvar(file1, "ctt")
precip1 = getvar(file1,'RAINC',-1)/25.4
precip2 = getvar(file1,'RAINNC',-1)/25.4
pressure = getvar(file1, "pressure")
z_comp = getvar(file1, "z", units="dm")
u_comp = getvar(file1, "ua", units="kt")
v_comp = getvar(file1, "va", units="kt")
wspd = getvar(file1, "wspd_wdir", units="kts")[0,:]


def getXY(Var):
	lats, lons = latlon_coords(Var)
	return lats, lons
def getBounds(Var):
	bounds = geo_bounds(Var)
	return bounds
	
	
	
zeta = '''
'Enter Which Model Number you want
1. Temperature Map of US
2. Pressure Map of US
3. Precip (dbz)
4. Pressure @ any mb
5. Wind Speed and Direction @any mb
6. Precipitation
7. Pressure of NY @ any mb with winds
8. Input Lats and Lons to get a Pressure map at the location of the mb you want
9. Global Cloud Top Temp
10.Temp Map in F
'''


a = input(zeta)

if a == ('1'):
	lats, lons = getXY(temp)
	#bounds = getBounds(temp)
	cart_projTemp = get_cartopy(temp)
	smoothTemp = smooth2d(temp, 2)
	TempMap(file1, td, cart_projTemp, time, lats, lons, smoothTemp)
elif a == ('2'):
	lats, lons = getXY(slp)
#	bounds = getBounds(slp)
	cart_projslp = get_cartopy(slp)
	smoothslp = smooth2d(slp, 2)
	SLPMap(file1, slp, cart_projslp, time, lats, lons, smoothslp)
elif a == ('4'):
	lats, lons  = getXY(pressure)
	PressureMap(file1, pressure, time, lats, lons, z_comp, u_comp, v_comp, wspd)
elif a ==('5'):
	lats, lons  = getXY(pressure)
	WindMap(file1, pressure, time, lats, lons, z_comp, u_comp, v_comp, wspd)
elif a == ('6'):
	lats, lons = getXY(p)
	#bounds = getBounds(temp)
	cart_projdbz = get_cartopy(p)
	Precip(file1, cart_projdbz, time, lats, lons, p, temp3d, qv, qr, slp)
elif a == ('7'):
	lats, lons = getXY(pressure)
	PressureMapPrecisely(file1, pressure, time, lats, lons, z_comp, u_comp, v_comp, wspd)
elif a == ('8'):
	lats, lons = getXY(pressure)
	PressureMapChooser(file1, pressure, time, lats, lons, z_comp, u_comp, v_comp, wspd)
elif a == ('9'):
	lats, lons = getXY(cloudTT)
	CTT(file1, cloudTT, time, lats, lons)
elif a == ('3'):
        lats, lons = getXY(precip1)
        #bounds = getBounds(temp)
        cart_projdbz = get_cartopy(p)
        Precip2(file1, cart_projdbz, time, lats, lons, p, precip1, precip2, slp)
if a == ('10'):
        lats, lons = getXY(tempF)
        bounds = getBounds(tempF)
        cart_projTemp = get_cartopy(temp)
        smoothTemp = smooth2d(tempF, 2)
        TempMap2(file1, tempF, cart_projTemp, time, lats, lons, smoothTemp,bounds)
