import netCDF4 as nc4
import numpy as np 
#-
def nearidx(lat,lon,xlat,xlon):
    lenlat = len(lat) 
    lenlon = len(lon)
    rmax = 1000000.0 
    for iilat in range(0,lenlat):
        for iilon  in range(0,lenlon):
        	xmax = np.sqrt( (lon[iilon] - xlon)**2 + (lat[iilat] - xlat) ** 2 ) 
        	if (xmax <= rmax):
        		irow = iilat
        		icol = iilon
        		rmax = xmax
    print (irow)
    return irow, icol